﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DefaultLiteralExpressions
{
    class Program
    {
        static async Task Main(string[] args)
		{
			await DoSomething();
		}

		public static async Task DoSomething(CancellationToken ct = default)
		{
			await Task.Run(() => Console.WriteLine("Hello World!"), ct);
		}
    }
}
