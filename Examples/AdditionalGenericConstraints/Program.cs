﻿using System;
using static System.Console;

namespace AdditionalGenericConstraints
{
    enum MyEnum
    {
        None = 0,
        SomeValue1 = 1
    }

    class Program
    {
        delegate int PerformCalculation(int x, int y);

        static void Main(string[] args)
        {
            PerformCalculation calculateDelegate = CalculateDelegate;

			MyEnum myEnum = MyEnum.SomeValue1;
			SomeMethod(calculateDelegate, myEnum);
        }

        static int CalculateDelegate(int x, int y)
		{
			return x + y;
		}

        static void SomeMethod<D, E>(D d, E e)
			where D: Delegate
			where E: Enum
		{
			WriteLine($"Delegate result: {d.DynamicInvoke(10, 15)}");
			WriteLine($"Enum value: {e.ToString()}");
		}
    }
}
