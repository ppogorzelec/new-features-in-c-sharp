﻿using static System.Console;

namespace RefReadonly
{
    static class Program
    {
        static void Main(string[] args)
		{
			int x = 1, y = 10;
			ref readonly int z = ref x.OrMaybe(in y);

			WriteLine($"x = {x}, y = {y}, z = {z}");
		}

		static ref readonly int OrMaybe(this in int x, in int y)
		{
			return ref x;
		}
    }
}
