﻿using System;

namespace GenericPatternMatching
{
    interface IWeapon { }
	interface IEnemy { }
	class Sword : IWeapon{ }
	class Bow : IWeapon { }
	class Bugzy : IEnemy { }

    class Program
    {
        static void Main(string[] args)
        {
            var weapon = new Sword();
			var enemy = new Bugzy();

			Attack(weapon, enemy);
        }

        static void Attack<T>(T weapon, IEnemy enemy)
			where T : IWeapon
		{
			switch (weapon)
			{
				case Sword sword:
					// process sword attack
					break;
				case Bow bow:
					// process bow attack
					break;
			}
		}
    }
}
