﻿using System.Linq;
using System.Threading.Tasks;
using static System.Console;

namespace DigitalSeparatorAfterBaseSpecifier
{
    class Program
	{
		static async Task Main(string[] args)
		{
			int[] numbers = { 0b_1, 0b_10, 0b_100, 0b_1000 };
			int result = await SumOfSquareAsync(numbers);
			WriteLine(result);
		}

		async static Task<int> SumOfSquareAsync(int[] numbers)
		{
			return await Task.Run(Compute);
			int Compute() => numbers.Select(i => i * i).Sum();
		}
	}
}
